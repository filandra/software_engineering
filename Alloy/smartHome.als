// --- Smart Home ---
// --- Adam Filandr ---


// --- Definitions ---

// Definition of base IotDevice
abstract sig IotDevice {
	connectedTo: lone IotDevice,
	authority: one Int
}

// Definition of base Controller
abstract sig Controller extends IotDevice{
	sensors: set Sensor,
	ids: set InteractiveDevice,
} {authority > 0}


// Definition of base InteractiveDevice
abstract sig InteractiveDevice extends IotDevice {
	controlledBy: set Controller,
	room: one Room
} {authority > 0}

// Definition of base Sensor
abstract sig Sensor extends IotDevice{
	controlledBy: set Controller,
	room: one Room
} {authority > 0}

// Definition of the main Router
one sig Router extends IotDevice{
	controllers: set Controller,
	ids: set InteractiveDevice,
	sensors: set Sensor
} {connectedTo = this && authority = 4}

// --- Security devices ---

sig SmartDoorLock extends InteractiveDevice {
} {room.smartDoorLock = this
	authority = 3}

sig SmartCamera extends InteractiveDevice {
	person: Int
} {room.smartCamera = this
	person >= 0
	authority = 3}

// --- Utility devices ---

sig SmartCourtains extends InteractiveDevice {
	on_off : Int
} {room.smartCourtains = this
	on_off >= 0 && on_off =< 1
	authority = 2}

sig SmartLightBulb extends InteractiveDevice {
	on_off : Int
} {room.smartLightBulb = this
	on_off >= 0 && on_off =< 1
	authority = 2}

// --- Sensor devices ---

sig ThermoMeter extends Sensor {
	temperature : Int
} {room.thermoMeter = this
	temperature >= 0 && temperature =< 50
	authority = 1}

sig PhotoSensor extends Sensor {
	brightness: Int
} {room.photoSensor = this
	brightness >= 0 && brightness =< 100
	authority = 1}

// --- Controllers ---

one sig AdministratorController extends Controller {
} {authority = 3}

one sig AdultController extends Controller {
} {authority = 2}

one sig ChildrenController extends Controller {
} {authority = 1}

// Definition of a room with people and security, utility and sensor devices
sig Room {
	person: Int,

	smartDoorLock: lone SmartDoorLock,
	smartCamera: one SmartCamera,

	smartCourtains: lone SmartCourtains,
	smartLightBulb: lone SmartLightBulb,

	thermoMeter: lone ThermoMeter,
	photoSensor: lone PhotoSensor,
} {person >= 0}


// --- Rules ---

// Connection between the Router and the Controllers.
fact {all r: Router, c: r.controllers | c.connectedTo = r}
// Connection between the Router and the interactiveDevices.
fact {all r: Router, id: r.ids | id.connectedTo = r}
// Connection between the Router and the Sensors.
fact {all r: Router, s: r.sensors | s.connectedTo = r}

// Each Controller is reachable from the Router.
fact {Controller in Router.*controllers}
// Each InteractiveDevice is reachable from the Router.
fact {InteractiveDevice in Router.*ids}
// Each Sensor is reachable from the Router.
fact {Sensor in Router.*sensors}

// Connection between the Controllers and the attached Sensors.
fact {all c: Controller, s: c.sensors | c in s.controlledBy}
// Connection between the Controllers and the attached InteractiveDevices.
fact {all c: Controller, id: c.ids | c in id.controlledBy }
// Ensure that Controllers controll their InteractiveDevices
fact {all id: InteractiveDevice | Controller in id.controlledBy}
// Ensure that Controllers controll their Sensors
fact {all s: Sensor | Controller in s.controlledBy}

// Each sensor is connected to a controller whose authority matches the sensor's authority
// This simulates access rights to controll the devices
// E.g. only administrator can controll the security devices
// Children can only controll the sensors
fact {all c: Controller, s: c.sensors | s.authority <= c.authority}

// We only focus on situations when a person is in the house
fact {some r: Room | r.person > 0}
// Number of people in camera is equal to number of people in the room
fact {all r: Room, cam: r.smartCamera | r.person = cam.person}

// Person walks from the view of one camera to the view of another one
pred movement [cam1, cam1', cam2, cam2': SmartCamera] {
	cam1.person > 0
	cam2'.person = cam2.person + 1
	cam1'.person = cam1.person - 1
}

// If there is a person in a room, or the room is not bright enough lights are turned on
fact lightsActivate {all r: Room, p: r.photoSensor, l : r.smartLightBulb | r.person > 0 && p.brightness < 50 => l.on_off = 1}
// If there is no person in a room, or the room is bright enough, lights are turned off
fact lightsDeactivate {all r: Room, p: r.photoSensor, l : r.smartLightBulb | r.person = 0 || p.brightness >= 50 => l.on_off = 0}

// If the temperature is too high, close the curtains
fact curtainsActivate {all r: Room, t : r.thermoMeter, c : r.smartCourtains | t.temperature > 25 => c.on_off = 1}
// If the temperature is too low, open the curtains
fact curtainsDeactivate {all r: Room, t : r.thermoMeter, c : r.smartCourtains | t.temperature <= 25 => c.on_off = 0}


// --- System checks ---

pred smartHome{}
run smartHome for 20 but exactly 3 Room

// Check if lights turn on and off accordingly
assert movement_activation {
	all room1, room2: Room, cam1, cam1': room1.smartCamera, cam2, cam2': room2.smartCamera |
	room1.person = 1 && room2.person = 0 && 
	movement[cam1, cam1', cam2, cam2'] => room2.smartLightBulb.on_off = 1 && room1.smartLightBulb.on_off = 0
}

check movement_activation expect 0

// Everything is connected to Router (router is connected to itself) and the system has only one such device
assert router {all d: IotDevice | one r: IotDevice | d.connectedTo = r}
check router for 20 expect 0

// Administrator controller has acces to all interactive devices
assert adminAcces_InteractiveDevices {all id: InteractiveDevice | one c: Controller | c.authority = 3 && c in id.controlledBy}
check adminAcces_InteractiveDevices for 20 expect 0

// Administrator controller has acces to all sensors
assert adminAcces_Sensors {all s: Sensor | one c: Controller | c.authority = 3 && c in s.controlledBy}
check adminAcces_Sensors for 20 expect 0

// Lights in the room activate, when the room is not bright enough
assert brightness_activation {all r: Room, p: r.photoSensor, l : r.smartLightBulb | p.brightness = 25 => l.on_off = 1}
check brightness_activation for 20 expect 0